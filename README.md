# GIMP CI Documentation

This is centralized documentation which helps to onboard a new maintainer or
contributor to the [GIMP CI system hosted at `build.gimp.org`][build].  The GIMP
CI system sole purpose is to provide regular automatic builds of [GIMP][gimp].

- Learn more [about hosting provided for GIMP Jenkins](hosting.md) which is
  sponsored by [FlamingText][ft].

# Project goals

Learn about [GIMP CI project goals](goals.md).  The project goals also define
concepts around the automation applied to GIMP CI.  The "CI" in GIMP CI is
"continuous integration" which is also defined in project goals.

# Architecture

![GIMP CI diagram](diagram/GIMP-CI-achitecture.png)

From the top down:

- https://build.gimp.org/ is using Jenkins to build GIMP and its dependencies.
- Each build occurs inside of an ephemeral [Docker][docker] container.  A new
  container is provisioned to build the software.  After the software is
  finished building, the container is deleted.  This ensures a fresh environment
  for every build.  Each dependency and GIMP itself is built inside of the [GIMP
  Development Environment][gimp-image] Docker image.
- Built in order within separate containers: `BABL` -> `GEGL` -> `libmypaint` ->
  `mypaint-brushes` -> `GIMP`.

# Project repositories

The following repositories are important for achieving GIMP CI project goals.

- [jenkins][repo-jenkins] provides IaC and CaC for GIMP CI.  This provides
  [Jenkins][jenkins] software packages installed on [build.gimp.org][build].
  Jenkins master settings are also stored as code in this repository.  A local
  version of Jenkins can be provisioned for development using this repository.
- [jenkins-dsl][repo-jenkins-dsl] provides CaC for GIMP CI.  Job configurations
  are hosted in this repository.  Additionally, GIMP build pipelines are also
  stored inside of this project which utilizes `docker-gimp` to build GIMP and
  its dependencies.
- [docker-gimp][repo-docker-gimp] provides IaC for GIMP CI.  This repository
  contains [Docker][docker] images which are meant to build GIMP.  The Docker
  images are used by [build.gimp.org][build] to provision a fresh environment
  for a build and completely delete the environment after the build is finished.
  This ensures a reliable and repeatable testing environment for building GIMP.

# Development guide

A guide to locally developing Jenkins is available in
[develop-locally.md](develop-locally.md).

# Reporting issues

See [reporting issues](reporting-issues.md) document.

[build]: https://build.gimp.org/
[docker]: https://www.docker.com/what-docker
[ft]: http://www.flamingtext.com/
[gimp-image]: https://hub.docker.com/r/gimp/gimp/
[gimp]: https://www.gimp.org/
[jenkins]: https://jenkins.io/
[repo-docker-gimp]: https://gitlab.gnome.org/World/gimp-ci/docker-gimp
[repo-jenkins-dsl]: https://gitlab.gnome.org/World/gimp-ci/jenkins-dsl
[repo-jenkins]: https://gitlab.gnome.org/World/gimp-ci/jenkins
