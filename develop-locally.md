# Locally developing build.gimp.org

This document instructs how one can develop build.gimp.org locally by using
[Vagrant][vagrant] and [VirtualBox][vbox] to assist with provisioning.  Before
beginning here are some recommendations:

- All development should occur outside of the Vagrant VM.  Mainly because
  vagrant makes it really easy to accidentally destroy work.
- [Fork the project][create-fork] you wish to develop and reference your fork in
  [Jenkins configurations][jenkins-config].

# Fork and clone

Only fork what you need.  For example, if you plan to develop only
[jenkins-dsl][jenkins-dsl], then fork that project to your GitLab user
namespace.

    git clone https://gitlab.gnome.org/samrocketman/jenkins-dsl.git

Replace `samrocketman` with your own GNOME GitLab username.

# Provision locally

### Clone jenkins repo

The [jenkins][jenkins] project is what provisions a local copy of
https://build.gimp.org/.

```bash
git clone --recursive https://gitlab.gnome.org/World/gimp-ci/jenkins.git
cd jenkins/
```

### Update jenkins repo to use your fork

Patch the following files so that the provisioned Jenkins uses your fork of
[jenkins-dsl][jenkins-dsl].

```patch
diff --git a/configs/job_generator.xml b/configs/job_generator.xml
index 206ae71..cfcb778 100644
--- a/configs/job_generator.xml
+++ b/configs/job_generator.xml
@@ -9,7 +9,7 @@
     <configVersion>2</configVersion>
     <userRemoteConfigs>
       <hudson.plugins.git.UserRemoteConfig>
-        <url>https://gitlab.gnome.org/World/gimp-ci/jenkins-dsl.git</url>
+        <url>https://gitlab.gnome.org/samrocketman/jenkins-dsl.git</url>
       </hudson.plugins.git.UserRemoteConfig>
     </userRemoteConfigs>
     <branches>
diff --git a/configs/shared-pipelines.groovy b/configs/shared-pipelines.groovy
index 0e02bdd..e57153d 100644
--- a/configs/shared-pipelines.groovy
+++ b/configs/shared-pipelines.groovy
@@ -5,7 +5,7 @@ pipeline_shared_libraries = [
         'allowVersionOverride': false,
         'includeInChangesets': true,
         'scm': [
-            'remote': 'https://gitlab.gnome.org/World/gimp-ci/jenkins-dsl.git'
+            'remote': 'https://gitlab.gnome.org/samrocketman/jenkins-dsl.git'
         ]
     ]
 ]
```

Replace `samrocketman` in the above example with your own GNOME GitLab
username.

### Bootstrap Jenkins

From the `jenkins` repository root run the following commands.

```bash
export VAGRANT_JENKINS=1
vagrant up
./jenkins_bootstrap.sh
```

### Developing

Push to the `master` branch of your fork.  Vagrant Jenkins will use updates from
your fork of [jenkins-dsl][jenkins-dsl].

### Contributing back

When you're ready, open a pull request following the [CONTRIBUTING
guidelines](reporting-issues.md).

[create-fork]: https://gitlab.gnome.org/help/gitlab-basics/fork-project.md
[jenkins-config]: https://gitlab.gnome.org/World/gimp-ci/jenkins/tree/master/configs
[jenkins-dsl]: https://gitlab.gnome.org/World/gimp-ci/jenkins-dsl
[jenkins]: https://gitlab.gnome.org/World/gimp-ci/jenkins
[vagrant]: https://www.vagrantup.com/
[vbox]: https://www.virtualbox.org/
