# Reporting general issues

For general infrastructure issues, open an issue with
[GIMP project](https://gitlab.gnome.org/GNOME/gimp).

- Add issue label `5. Jenkins`.
- Assign the issue to [@samrocketman][me].

A general issue includes GIMP CI outages, a missing dependency for building GIMP
within the CI infrastructure, and so on.

# Reporting project-specific issues

For reporting an issue to one of the [project repositories](README.md), refer to
the `CONTRIBUTING.md` document in each repository.

[me]: https://gitlab.gnome.org/samrocketman
