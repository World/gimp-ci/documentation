# Project Goals

- Make configuration of the [GIMP CI system][build] transparent for developers
  and contributors.
- Provide a means for contributors of [GIMP][gimp] to easily provision a
  development environment for building GIMP.
- Make use of modern technologies and concepts such as **infrastructure as
  code**, **configuration as code**, **continuous integration**, **continuous
  delivery**, and **continuous deployment**.

The above goals will be met when a contributor is able to:

1. Fully provision a Jenkins build system for building GIMP.
2. Fully provision a GIMP development environment for contributing to GIMP.

# Shorthand terms

| Shorthand | Term                           |
| --------- | ------------------------------ |
| IaC       | Infrastructure as Code         |
| CaC       | Configuration as Code          |
| CI        | Continuous Integration         |
| CD        | Continuous delivery            |
| CD        | Continuous deployment          |
| DSL       | Domain Specific Language       |
| GIMP      | GNU Image Manipulation Program |

# What is infrastructure as code?

Infrastructure as Code (IaC for short) is the concept of using a combination of
configuration management and automated provisioning tools.  Imagine a scenario
where a lot of effort is put into configuring a single server.  Now imagine
having to configure that same server hundreds of times.  Infrastructure as code
aims to make provisioning and configuring servers easy.

GIMP CI relies on the following infrastructure as code technologies.

- [Ansible][ansible] - configures hosting.
- [Docker][docker] - Machine images for building GIMP and all of its
  dependencies.
- [Git][git] - Tracks changes in code and documentation as infrastructure is
  developed.

# What is configuration as code?

Configuration as code (CaC for short) is the concept of storing all settings for
an application in revision control.  Rather than logging into an application and
configuring its settings via its GUI, configuration as code stores settings in a
repository where changes to configuration can be tracked and reviewed.
Configuration as code is often abstracted into a domain specific language (DSL
for short).

GIMP CI relies on the following configuration as code technologies.

- [Jenkins script console][script-console] - A [Groovy][groovy]-based automation
  endpoint in Jenkins which admins can use to troubleshoot and configure
  Jenkins.
- [Jenkins pipeline as code][pipeline] - A [Groovy][groovy]-based way to
  describe software build flows in Jenkins.
- [Jenkins Job DSL][job-dsl] - A [Groovy][groovy]-based way to describe Jenkins
  job configurations.
- [jenkins-bootstrap-shared][jenkins-bootstrap] - A shared repository of groovy
  scripts which centralizes configuration as code scripts.  The scripts are
  meant to be run by an admin in the script console.

# What is continuous integration?

Continuous integration (CI for short) is a conceptual process.  The idea is to
have a shared central repository of code.  Developers develop their work on
local machines and regularly **integrate** their work into the shared
repository.  Sometimes, there is CI automation which helps make integrating work
more stable (but is not necessarily required).  The **CI** in GIMP CI comes from
the words continuous integration.  In order to properly integrate work into a
shared repository there must be a way to validate the work proposed.  Automated
tests must be available in order to regularly validate work.  Tests can include
[Unit tests][test-unit], [Integration tests][test-integration] (not the same as
CI), or [Load tests][test-load] to name a few.

GIMP CI relies on the following continuous integration technologies.

- Development workflows are part of CI.  There are a few popular development
  workflows: [git flow][flow-git], [GitHub flow][flow-github], and more recently
  [GitLab flow][flow-gitlab].  Currently, GitHub flow is used since it is the
  simplest.
- [Jenkins CI][jenkins] - an automation server used to validate regularly
  integrated work.  Jenkins can also be used to validate merge requests before
  they're merged.  This helps to make master branch development more stable.
- [Git][git] tracks changes in code and documentation.
- [GitLab][gitlab] serves as the central hosting repository where work is
  regularly integrated.  Specifically, [GitLab hosted by GNOME][gitlab-gnome].

# What is continuous delivery?

Continuous delivery is where the state of a branch in a repository is always
"production ready".  That means the branch is stable enough that it can be built
and released to production at any commit in that branch.

Continuous delivery (CD for short) is a conceptual process.  It's an idea around
regularly delivering work to production.  In this case, "production" means where
users actually use the software.  GIMP delivers to production when the GIMP
development teams release a stable version of GIMP for general use.  GIMP CI
delivers to production every time settings are updated or a new version of
Jenkins is released on [build.gimp.org][build].

How is continuous delivery different from regular software delivery?  Regular
software delivery has no requirements around releasing software.  For example,
the development branches of GIMP and its dependencies may or may not be stable.
Continuous delivery does require a branch where the state of any given commit is
stable for release.

# What is continuous deployment?

Continuous deployment (also CD for short, confusingly) is the concept where
merging code into a production branch automatically releases it to end users
with no human interaction once the change is merged.

In order for a project to achieve continuous delivery in a **stable** manner, it
must combine all of the concepts mentioned up to this point.

[ansible]: https://www.ansible.com/
[build]: https://build.gimp.org/
[docker]: https://www.docker.com/
[flow-git]: https://github.com/nvie/gitflow
[flow-github]: https://guides.github.com/introduction/flow/
[flow-gitlab]: https://docs.gitlab.com/ee/workflow/gitlab_flow.html
[gimp]: https://www.gimp.org/
[git]: https://git-scm.com/
[gitlab-gnome]: https://gitlab.gnome.org/
[gitlab]: https://about.gitlab.com/
[groovy]: http://groovy-lang.org/
[jenkins-bootstrap]: https://github.com/samrocketman/jenkins-bootstrap-shared
[jenkins]: https://jenkins.io/
[job-dsl]: https://github.com/jenkinsci/job-dsl-plugin/wiki
[pipeline]: https://jenkins.io/solutions/pipeline/
[script-console]: https://wiki.jenkins.io/display/JENKINS/Jenkins+Script+Console
[test-integration]: https://en.wikipedia.org/wiki/Integration_testing
[test-load]: https://en.wikipedia.org/wiki/Load_testing
[test-unit]: https://en.wikipedia.org/wiki/Unit_testing
