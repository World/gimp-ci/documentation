# About build.gimp.org

* Point of Contact: [Sam Gleske `@samrocketman`][me]
* Sponsors: [FlamingText][ft] provides hardware for the build system and
  majority of bandwidth.  [The GNOME Foundation][gnome] provides the HTTP load
  balancer.

There is an HTTP load balancer in front of GIMP build server controlled by the
[GNOME infrastructure team][gnome-infra].  SSL is terminated by this load
balancer.

On the back end, the Jenkins build system lives on a virtual machine provided by
[FlamingText][ft].  Host is CentOS 6 and the guest is Debian Testing.  GIMP
developers prefer to develop GIMP on [Debian Testing][debian] so that is the OS
of choice for the GIMP CI system.

Hardware specs:

* CPU: Intel(R) Core(TM) i7 CPU 870 @ 2.93GHz stepping 05 (6 cores available to
  guest)
* RAM: 8GB (4GB available to guest)
* Disk: 2x500GB HDD (150GB available to guest)
* Virtualization Layer: VMWare
* Network speed: Gigabit LAN; dual 100BaseT WAN.

Jenkins is installed via the [`jenkins-bootstrap` debian
package][jenkins-debian].

[build]: https://build.gimp.org/
[debian]: https://www.debian.org/releases/
[ft]: http://www.flamingtext.com/
[gnome-infra]: https://mail.gnome.org/mailman/listinfo/gnome-infrastructure
[gnome]: https://www.gnome.org/foundation/
[jenkins-debian]: https://gitlab.gnome.org/World/gimp-ci/jenkins
[me]: https://gitlab.gnome.org/samrocketman
